#pragma once

#include "fmt/format.h"
#include <concepts>
#include <optional>
#include <string>
#include <type_traits>

#ifdef LK_RESULT_USE_FMT
#include <fmt/core.h>
#endif

namespace lk {

struct Error {
    Error(const std::string& str)
        : m_err { str } {
    }

#ifdef LK_RESULT_USE_FMT
    template<typename... Args>
    Error(std::string_view str, Args&&... args)
        : m_err(fmt::format(fmt::runtime(str), args...)) {
    }
#endif

    std::string error() const {
        return m_err;
    }

private:
    std::string m_err;
};

template<typename T, typename E>
struct GenericResult final {
    using error_type = E;
    using value_type = T;

    GenericResult() = default;

    GenericResult(const T& value) {
        with_value(value);
    }

    GenericResult(const Error& error_val) {
        with_error(error_val.error());
    }

    template<typename OT>
    requires(!std::same_as<OT, Error> && !std::same_as<OT, E> && !std::same_as<OT, T> && std::constructible_from<T, OT>)
        GenericResult(const OT& value) {
        with_value(value);
    }

    ~GenericResult() = default;
    GenericResult(GenericResult&) = delete;
    GenericResult(GenericResult&&) noexcept = default;

    GenericResult& with_error(const E& error) {
        m_error = error;
        m_value.reset();
        return *this;
    }

    GenericResult& with_error(E&& error) {
        m_error = std::move(error);
        m_value.reset();
        return *this;
    }

    GenericResult& with_value(const T& value) {
        m_error = {};
        m_value = value;
        return *this;
    }

    GenericResult& with_value(T&& value) {
        m_error = {};
        m_value = std::move(value);
        return *this;
    }

    template<typename OE>
    requires(!std::same_as<E, OE> && std::constructible_from<E, OE>) GenericResult& with_error(const OE& error_val) {
        return with_error(E(error_val));
    }

    template<typename OT>
    requires(!std::same_as<T, OT> && std::constructible_from<T, OT>) GenericResult& with_value(const OT& value) {
        return with_value(T(value));
    }

    static GenericResult Error(const E& error) {
        return GenericResult { error, {} };
    }

    template<typename OE>
    requires(!std::same_as<E, OE>) static GenericResult Error(const OE& error) {
        return GenericResult { E(error), {} };
    }

    static GenericResult Ok(const T& value) {
        return GenericResult { {}, value };
    }

    template<typename OT>
    requires(!std::same_as<E, OT>) static GenericResult Ok(const OT& value) {
        return GenericResult { {}, T(value) };
    }

    T& value() { return m_value.value(); }
    const T& value() const { return m_value.value(); }

    T& value_or(T& other) { return has_value() ? value() : other; }
    const T& value_or(const T& other) const { return has_value() ? value() : other; }

    operator bool() const { return has_value(); }
    bool has_value() const { return m_value.has_value(); }

    const E& error() const { return m_error; }
    E& error() { return m_error; }

private:
    E m_error;
    std::optional<T> m_value;
};

template<typename T>
using Result = GenericResult<T, std::string>;

template<typename T>
auto Ok(const T& value) {
    return Result<T> { {}, value };
}

}
